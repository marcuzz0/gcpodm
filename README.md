# gcpodm

## Cos'è

gcpodm è uno script shell scritto in bash che permette la trasformazione e successiva formattazione di un file con estensione *.txt formattato come Nome,Latitudine,Longitudine,Quota in un file *.txt adatto per essere importato in WebODM insieme al dataset fotografico per la successiva elaborazione SFM.

## Installazione e dipendenze
```
sudo apt update && sudo apt upgrade -y
sudo apt install proj-bin -y
sudo apt install gdal-bin -y
git clone https://gitlab.com/marcuzz0/gcpodm.git
cd gcpodm
sudo chmod +x gcpodm
cp gcpodm $USER/bin
```

## Come funziona

Da terminale lanciare lo script passandogli dei parametri, ad esempio:
```
./gcpodm -e 3003 -f file.txt
```
La sintassi consentita è la seguente:
- -h stampa help
- -e permette fi inserire EPSG
- -f permette di inserire il path al file

Lo script se eseguito correttamente restituisce un file nella medesima cartella avente le seguenti caratteristiche XXXX.odm.XXXX.txt.
Viene mantenuto il nome ed il formato del file di partenza aggiungendogli il suffisso "odm" e il codice EPSG scelto per la trasformazione.

PS Per il sistema operativo Windows è necesaario aver precedentemente installato il sottosistema WSL.
